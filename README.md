### UX Process

Okay so these are the steps that should be followed before begining to build a new site that could be considered a large project. 
Anything where the user has to go beyond 3 steps to do anything could be considered 'large'



## Step One – Understand competitors and potential users.

1. Competitive Analysis
    - Analysing the strengths, weakness, and value propositions made by direct and indirect competitors.
    - A typical review or test focuses on 2 to 4 competitors’ sites. Any more than that can be too expensive and too overwhelming to analyze.
    - Ecommerce merchants who take the time to examine what their competition does online are better positioned to improve the performance and search engine rankings of their own site.
    - Quick online guides [here](https://www.nngroup.com/articles/competitive-usability-evaluations/), [here](http://www.practicalecommerce.com/How-to-Analyze-Your-Ecommerce-Competitors), [here](https://minerva.leeds.ac.uk/bbcswebdav/orgs/SCH_Computing/MSCProj/reports/0001/lee.pdf), and [here](https://www.youtube.com/watch?v=vSx2wayTk0g)
    - The competitive analysis process is a bit frontend loaded, meaning that it will take more time to complete the first time you do it than it will each time you update it.
    - Going through each competitor and and evaluaating the following is a miniumum:
        - The Checkout Process
        - Product Descriptions
        - Site headline + Tagline
        - General Style
        - Roadblocks to purchase
2. Recomendations
    - What the competitors do right.
    - What the competitors do wrong.
    - What the current site build should set out to accomplish.
3. User Personas
    - Define:
        - Who the customers are.
        - Their frustrations.
        - Needs and wants.
    - Examples:
        - [Usability.gov](https://www.usability.gov/how-to-and-tools/methods/personas.html)
        - [UXPin](https://www.uxpin.com/studio/blog/the-practical-guide-to-empathy-maps-creating-a-10-minute-persona/)
4. User scenarios
    - Determine possible scenarios the users could get in to.
    - Identify how these issues can be resolved/prevented.


## Step Two - Define thye information arcitechture

1. Sitemap
2. Content Model
3. Card Sorting
4. User Task flows